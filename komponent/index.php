<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Компонент");

$custom_filter = array(
            "IBLOCK_ID"=>1,
            "ACTIVE_DATE"=>"Y",
            "ACTIVE"=>"Y",
            array(
                "LOGIC" => "OR",
                array("SECTION_ID" => 0),
                array("!SECTION_ID" => 0, "SECTION_GLOBAL_ACTIVE"=>"Y"),
            ),
		);
?>
 <?$APPLICATION->IncludeComponent(
	"capybara:async_news_list", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "1",
		"ADDITIONAL_FILTER" => $custom_filter,
		"PAGE_NUM" => "1",
		"NEWS_NUM_ALL" => "5",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"SORT_CUSTOM" => ""
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>