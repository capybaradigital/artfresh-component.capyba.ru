<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;

if(Loader::includeModule('iblock')) {

    $elementOrder = [
        'SORT' => 'DESC'
    ];
    $elementFilter = [
        'IBLOCK_ID' => 1,
        'ACTIVE' => 'Y'
    ];
    $elementSelect = [
        'ID',
        'NAME'
    ];

    $elementIterator = CIBlockElement::GetList($elementOrder, $elementFilter, false, false, $elementSelect);
    $elements = [];
    while ($elementData = $elementIterator->GetNext()) {
        $elements[] = $elementData;
    }

    echo '<pre>';
    print_r($elements);
    echo '</pre>';

} else {
    ShowError('Модуль &laquo;Информационные блоки&raquo; не подключен');
    return;
}