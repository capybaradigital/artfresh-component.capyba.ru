<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
    "NAME" => Loc::getMessage('CAPYBARA_COMPONENT_NAME'),
    "DESCRIPTION" => Loc::getMessage('CAPYBARA_COMPONENT_DESCRIPTION'),
    "SORT" => 23,
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "capybara",
        "NAME" => Loc::getMessage('CAPYBARA_COMPONENT_SECTION'),
    ),
);