<?php

$MESS['CAPYBARA_IBLOCK_ID'] = 'ID инфоблока';
$MESS['CAPYBARA_ELEMENTS_COUNT'] = 'Номер страницы для постраничной навигации';
$MESS['CAPYBARA_PAGER_NUMBER'] = 'Количество выводимых новостей на странице';
$MESS['CAPYBARA_SORT_BY'] = 'Поля сортировки';
$MESS['CAPYBARA_SORT_ORDER'] = 'Порядок сортировки';
