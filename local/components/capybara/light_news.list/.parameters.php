<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var string $componentPath
 * @var array $arCurrentValues
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\Component\Base;
use Bitrix\Main\Web\Json;

if (!Loader::includeModule('iblock')) {
    return;
}

$customFilter = $arCurrentValues['IBLOCK_ID'];

$arComponentParameters = array(
    'GROUPS' => array(),
    'PARAMETERS' => array(
        'IBLOCK_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('CAPYBARA_IBLOCK_ID'),
            'TYPE' => 'STRING',
            'DEFAULT' => '',
        ),
        'ELEMENTS_COUNT' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('CAPYBARA_ELEMENTS_COUNT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '10',
        ),
        'PAGER_NUMBER' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('CAPYBARA_PAGER_NUMBER'),
            'TYPE' => 'STRING',
            'DEFAULT' => '1',
        ),
        'CUSTOM_FILTER' => array(
            'PARENT' => 'BASE',
            'NAME' => 'Фильтр',
            'TYPE' => 'CUSTOM',
            'JS_FILE' => Base::getSettingsScript($componentPath, 'filter_conditions'),
            'JS_EVENT' => 'initFilterConditionsControl',
            'JS_MESSAGES' => Json::encode(['invalid' => 'Не корреткный']),
            'JS_DATA' => Json::encode($customFilter),
            'DEFAULT' => ''
        ),
        'SORT_BY' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('CAPYBARA_SORT_BY'),
            'TYPE' => 'LIST',
            'DEFAULT' => 'ACTIVE_FROM',
            'VALUES' => '',
            'ADDITIONAL_VALUES' => 'Y',
        ),
        'SORT_ORDER' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('CAPYBARA_SORT_ORDER'),
            'TYPE' => 'LIST',
            'DEFAULT' => 'DESC',
            'VALUES' => '',
            'ADDITIONAL_VALUES' => 'Y',
        ),
        'CACHE_TIME' => array('DEFAULT' => 36000000),
    ),
);
